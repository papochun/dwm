/* See LICENSE file for copyright and license details. */

/* appearance */
static const unsigned int borderpx       = 6;   /* border pixel of windows */
static const unsigned int snap           = 0;  /* snap pixel */
static const unsigned int gappih         = 0;  /* horiz inner gap between windows */
static const unsigned int gappiv         = 0;  /* vert inner gap between windows */
static const unsigned int gappoh         = 20;  /* horiz outer gap between windows and screen edge */
static const unsigned int gappov         = 20;  /* vert outer gap between windows and screen edge */
static const int smartgaps_fact          = 1;   /* gap factor when there is only one client; 0 = no gaps, 3 = 3x outer gaps */

static const char autostartblocksh[]     = "autostart_blocking.sh";
static const char autostartsh[]          = "autostart.sh";
static const char dwmdir[]               = "dwm";
static const char localshare[]           = ".local/share";

static const int showbar                 = 1;   /* 0 means no bar */
static const int topbar                  = 1;   /* 0 means bottom bar */
/* Status is to be shown on: -1 (all monitors), 0 (a specific monitor by index), 'A' (active monitor) */
static const int statusmon               = 'A';
static const unsigned int systrayspacing = 0;   /* systray spacing */
static const int showsystray             = 1;   /* 0 means no systray */



/* Indicators: see patch/bar_indicators.h for options */
static int tagindicatortype              = INDICATOR_TOP_LEFT_SQUARE;
static int tiledindicatortype            = INDICATOR_NONE;
static int floatindicatortype            = INDICATOR_TOP_LEFT_SQUARE;
static void (*bartabmonfns[])(Monitor *) = { monocle /* , customlayoutfn */ };
static const char *fonts[]               = { "iosevkanerdfontmono:pixelsize=24:antialiasing=true" };

static char c000000[]                    = "#000000"; // placeholder value

static char normfgcolor[]                = "#bbbbbb";
static char normbgcolor[]                = "#222222";
static char normbordercolor[]            = "#444444";
static char normfloatcolor[]             = "#db8fd9";

static char selfgcolor[]                 = "#eeeeee";
static char selbgcolor[]                 = "#005577";
static char selbordercolor[]             = "#005577";
static char selfloatcolor[]              = "#005577";

static char titlenormfgcolor[]           = "#bbbbbb";
static char titlenormbgcolor[]           = "#222222";
static char titlenormbordercolor[]       = "#444444";
static char titlenormfloatcolor[]        = "#db8fd9";

static char titleselfgcolor[]            = "#eeeeee";
static char titleselbgcolor[]            = "#005577";
static char titleselbordercolor[]        = "#005577";
static char titleselfloatcolor[]         = "#005577";

static char tagsnormfgcolor[]            = "#bbbbbb";
static char tagsnormbgcolor[]            = "#222222";
static char tagsnormbordercolor[]        = "#444444";
static char tagsnormfloatcolor[]         = "#db8fd9";

static char tagsselfgcolor[]             = "#eeeeee";
static char tagsselbgcolor[]             = "#005577";
static char tagsselbordercolor[]         = "#005577";
static char tagsselfloatcolor[]          = "#005577";

static char hidnormfgcolor[]             = "#005577";
static char hidselfgcolor[]              = "#227799";
static char hidnormbgcolor[]             = "#222222";
static char hidselbgcolor[]              = "#222222";

static char urgfgcolor[]                 = "#bbbbbb";
static char urgbgcolor[]                 = "#222222";
static char urgbordercolor[]             = "#ff0000";
static char urgfloatcolor[]              = "#db8fd9";



static char *colors[][ColCount] = {
	/*                       fg                bg                border                float */
	[SchemeNorm]         = { normfgcolor,      normbgcolor,      normbordercolor,      normfloatcolor },
	[SchemeSel]          = { selfgcolor,       selbgcolor,       selbordercolor,       selfloatcolor },
	[SchemeTitleNorm]    = { titlenormfgcolor, titlenormbgcolor, titlenormbordercolor, titlenormfloatcolor },
	[SchemeTitleSel]     = { titleselfgcolor,  titleselbgcolor,  titleselbordercolor,  titleselfloatcolor },
	[SchemeTagsNorm]     = { tagsnormfgcolor,  tagsnormbgcolor,  tagsnormbordercolor,  tagsnormfloatcolor },
	[SchemeTagsSel]      = { tagsselfgcolor,   tagsselbgcolor,   tagsselbordercolor,   tagsselfloatcolor },
	[SchemeHidNorm]      = { hidnormfgcolor,   hidnormbgcolor,   c000000,              c000000 },
	[SchemeHidSel]       = { hidselfgcolor,    hidselbgcolor,    c000000,              c000000 },
	[SchemeUrg]          = { urgfgcolor,       urgbgcolor,       urgbordercolor,       urgfloatcolor },
};



static char *tagicons[][NUMTAGS] =
{
	[DEFAULT_TAGS]        = { "W", "T", "G", "V" },
};


/* There are two options when it comes to per-client rules:
 *  - a typical struct table or
 *  - using the RULE macro
 *
 * A traditional struct table looks like this:
 *    // class      instance  title  wintype  tags mask  isfloating  monitor
 *    { "Gimp",     NULL,     NULL,  NULL,    1 << 4,    0,          -1 },
 *    { "Firefox",  NULL,     NULL,  NULL,    1 << 7,    0,          -1 },
 *
 * The RULE macro has the default values set for each field allowing you to only
 * specify the values that are relevant for your rule, e.g.
 *
 *    RULE(.class = "Gimp", .tags = 1 << 4)
 *    RULE(.class = "Firefox", .tags = 1 << 7)
 *
 * Refer to the Rule struct definition for the list of available fields depending on
 * the patches you enable.
 */
static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 *	WM_WINDOW_ROLE(STRING) = role
	 *	_NET_WM_WINDOW_TYPE(ATOM) = wintype
	 */
	RULE(.wintype = WTYPE "DIALOG", .isfloating = 1)
	RULE(.wintype = WTYPE "UTILITY", .isfloating = 1)
	RULE(.wintype = WTYPE "TOOLBAR", .isfloating = 1)
	RULE(.wintype = WTYPE "SPLASH", .isfloating = 1)
	RULE(.class = "Gimp", .tags = 1 << 4)
	RULE(.class = "Firefox", .tags = 1 << 7)
};



/* Bar rules allow you to configure what is shown where on the bar, as well as
 * introducing your own bar modules.
 *
 *    monitor:
 *      -1  show on all monitors
 *       0  show on monitor 0
 *      'A' show on active monitor (i.e. focused / selected) (or just -1 for active?)
 *    bar - bar index, 0 is default, 1 is extrabar
 *    alignment - how the module is aligned compared to other modules
 *    widthfunc, drawfunc, clickfunc - providing bar module width, draw and click functions
 *    name - does nothing, intended for visual clue and for logging / debugging
 */
static const BarRule barrules[] = {
	/* monitor   bar    alignment         widthfunc                 drawfunc                clickfunc                hoverfunc                name */
	{ -1,        0,     BAR_ALIGN_LEFT,   width_tags,               draw_tags,              click_tags,              hover_tags,              "tags" },
	{  0,        0,     BAR_ALIGN_RIGHT,  width_systray,            draw_systray,           click_systray,           NULL,                    "systray" },
	{ -1,        0,     BAR_ALIGN_LEFT,   width_ltsymbol,           draw_ltsymbol,          click_ltsymbol,          NULL,                    "layout" },
	{ statusmon, 0,     BAR_ALIGN_RIGHT,  width_status2d,           draw_status2d,          click_status2d,          NULL,                    "status2d" },
	{ -1,        0,     BAR_ALIGN_NONE,   width_bartabgroups,       draw_bartabgroups,      click_bartabgroups,      NULL,                    "bartabgroups" },
};

/* layout(s) */
static const float mfact     = 0.50; /* factor of master area size [0.05..0.95] */
static const int nmaster     = 1;    /* number of clients in master area */
static const int resizehints = 0;    /* 1 means respect size hints in tiled resizals */
static const int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */



static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[D]",      deck },
	{ "[M]",      monocle },
};


/* key definitions */
#define SUPER Mod4Mask
#define ALT Mod1Mask
#define SHIFT ShiftMask
#define CTRL ControlMask

#define TAGKEYS(KEY,TAG) \
	{ SUPER,                KEY,      view,           {.ui = 1 << TAG} }, \
	{ SUPER|CTRL,           KEY,      toggleview,     {.ui = 1 << TAG} }, \
	{ SUPER|SHIFT,          KEY,      tag,            {.ui = 1 << TAG} }, \
	{ SUPER|CTRL|SHIFT,     KEY,      toggletag,      {.ui = 1 << TAG} },



/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define EXEC(...) { .v = (const char*[]) {__VA_ARGS__, NULL }}


/* commands */
static const char *dmenucmd[] = { "dmenu_run", NULL };
static const char *termcmd[]  = { "st", NULL };

static const Key keys[] = {
    /*
	  modifier                      key             function               argument
    */
    { SUPER,                      XK_a,           spawn,                 EXEC("dmenu_run") },
    { SUPER,                      XK_Return,      spawn,                 EXEC("st") },
    { SUPER,                      XK_s,           spawn,                 EXEC("screenshot.sh") },
    { SUPER|SHIFT,                XK_s,           spawn,                 EXEC("screenshot.sh", "-s") },

    { SUPER|SHIFT,                XK_Right,       spawn,                 EXEC("cmus-remote", "-n") },
    { SUPER|SHIFT,                XK_Left,        spawn,                 EXEC("cmus-remote", "-r") },
    { SUPER,                      XK_Right,       spawn,                 EXEC("cmus-remote", "-k", "+5") },
    { SUPER,                      XK_Left,        spawn,                 EXEC("cmus-remote", "-k", "-5") },
    { SUPER,                      XK_Up,          spawn,                 EXEC("cmus-remote", "-v", "+5%") },
    { SUPER,                      XK_Down,        spawn,                 EXEC("cmus-remote", "-v", "-5%") },
    { SUPER,                      XK_space,       spawn,                 EXEC("cmus-remote", "-u") },

    { SUPER,                      XK_apostrophe,  spawn,                 EXEC("bluetoothctl", "power", "on") },
    { SUPER|SHIFT,                XK_apostrophe,  spawn,                 EXEC("bluetoothctl", "power", "off") },
    { SUPER,                      XK_f,           spawn,                 EXEC("feed.sh") },
    { SUPER,                      XK_Escape,      spawn,                 EXEC("setting.sh") },
    { SUPER,                      XK_v,           spawn,                 EXEC("view.sh") },
    { SUPER,                      XK_p,           spawn,                 EXEC("spectre.sh") },
    { SUPER|SHIFT,                XK_p,           spawn,                 EXEC("2fa.sh") },
    { SUPER,                      XK_r,           spawn,                 EXEC("refresh.sh") }, 


    { SUPER,                      XK_b,           togglebar,             {0} },
    { SUPER,                      XK_j,           focusstack,            {.i = +1 } },
    { SUPER,                      XK_k,           focusstack,            {.i = -1 } },
    { SUPER|SHIFT,                XK_h,           incnmaster,            {.i = +1 } },
    { SUPER|SHIFT,                XK_l,           incnmaster,            {.i = -1 } },
    { SUPER,                      XK_h,           setmfact,              {.f = -0.05} },
    { SUPER,                      XK_l,           setmfact,              {.f = +0.05} },
    { SUPER|SHIFT,                XK_Return,      zoom,                  {0} },
    { SUPER,                      XK_Tab,         view,                  {0} },
    { ALT,                        XK_Tab,         focusstack,            {0} },
    { SUPER|SHIFT,                XK_c,           killclient,            {0} },
    { SUPER|SHIFT,                XK_q,           quit,                  {0} },
    { SUPER|SHIFT,                XK_r,           quit,                  {1} },
    { SUPER|SHIFT,                XK_x,           xrdb,                  {.v = NULL } },
    { SUPER,                      XK_t,           setlayout,             {.v = &layouts[0]} },
    { SUPER,                      XK_m,           setlayout,             {.v = &layouts[1]} },
    { SUPER|SHIFT,                XK_space,       togglefloating,        {0} },
    { SUPER|SHIFT,                XK_f,           togglefullscreen,      {0} },
    { SUPER,                      XK_comma,       focusmon,              {.i = -1 } },
    { SUPER,                      XK_period,      focusmon,              {.i = +1 } },
    { SUPER|ShiftMask,            XK_comma,       tagmon,                {.i = -1 } },
    { SUPER|ShiftMask,            XK_period,      tagmon,                {.i = +1 } },

    /*
	  switching the keys to be above 'hjkl' makes it easier to switch layouts and do actions.
	  */
    TAGKEYS(XK_y, 0)
    TAGKEYS(XK_u, 1)
    TAGKEYS(XK_i, 2)
    TAGKEYS(XK_o, 3)
};


/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
    /*
	  click                   event mask           button          function        argument
	  */
    { ClkLtSymbol,          0,                   Button1,        setlayout,      {0} },
    { ClkLtSymbol,          0,                   Button3,        setlayout,      {.v = &layouts[2]} },
    { ClkWinTitle,          0,                   Button2,        zoom,           {0} },
    { ClkStatusText,        0,                   Button2,        spawn,          {.v = termcmd } },
    { ClkClientWin,         SUPER,               Button1,        movemouse,      {0} },
    { ClkClientWin,         SUPER,               Button2,        togglefloating, {0} },
    { ClkClientWin,         SUPER,               Button3,        resizemouse,    {0} },
    { ClkTagBar,            0,                   Button1,        view,           {0} },
    { ClkTagBar,            0,                   Button3,        toggleview,     {0} },
    { ClkTagBar,            SUPER,               Button1,        tag,            {0} },
    { ClkTagBar,            SUPER,               Button3,        toggletag,      {0} },
};
